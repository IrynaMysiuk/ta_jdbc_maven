package com.epam.lab;

import com.epam.lab.daoimpl.DepartmentDaoImpl;
import com.epam.lab.entities.Department;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DepartmentTests {
    private static int expectedId = 987;
    private static Logger logger = LogManager.getLogger(App.class);

    @Test
    public void checkInsertDepartment() {
        DepartmentDaoImpl ddi = new DepartmentDaoImpl();
        logger.info("Create table");
        ddi.createDepartmentTable();
        logger.info("Insert a new record");
        Department department = new Department("Fei", "electronics")
                .setId(expectedId);
        ddi.insert(department);
        logger.info("Select by id");
        Department departmentSelect = ddi.selectById(expectedId);
        logger.info(department.getId() + ", " + department.getSpecialityName() + ", "
                + department.getDescribeSpeciality());
        assertEquals("Employee was not added", expectedId, departmentSelect.getId());

    }

    @Test
    public void checkUpdateDepartment() {
        String expectedSpecialityName = "KH";
        Department department = new Department("Fei", "electronics");
        DepartmentDaoImpl ddi = new DepartmentDaoImpl();
        logger.info("Select by id");
        Department departmentSelect = ddi.selectById(expectedId);
        logger.info(department.getId() + ", " + department.getSpecialityName() + ", "
                + department.getDescribeSpeciality());
        Department departmentUpdate = departmentSelect.setSpecialityName(expectedSpecialityName);
        ddi.update(departmentUpdate, departmentSelect.getId());
        assertEquals("" + departmentUpdate.getSpecialityName(), expectedSpecialityName);
    }
    @AfterClass
    public static void deleteData() {
        DepartmentDaoImpl ddi = new DepartmentDaoImpl();
        ddi.delete(expectedId);
    }
}
