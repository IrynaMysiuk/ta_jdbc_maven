package com.epam.lab;

import com.epam.lab.daoimpl.EmployeeDaolmpl;
import com.epam.lab.entities.Employee;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EmployeeTests {
    private static Logger logger = LogManager.getLogger(App.class);
    private int expectedId = 967;
    private int initialEmployee;
    private EmployeeDaolmpl sdi;

    @Before
    public void addTestData(){
        sdi = new EmployeeDaolmpl();
        sdi.createEmployeeTable();
        initialEmployee = (int) Math.random();
        Employee employee = new Employee()
                .setId(initialEmployee)
                .setLastName("Googlay").setFirstName("Bohdan")
                .setMiddleName("Severynovych").setPoint("m")
                .setBirthdayDay("1935-12-01").setParentsName("Googlay Severyn Valentinovych")
                .setPhone("11122233").setPassport("CC0909CC")
                .setRecordBookNumber("3114029c").setArrivalDate("2011-09-01");
        sdi.insert(employee);
    }

    @Test
    public void checkInsertEmployee() {
        EmployeeDaolmpl sdi = new EmployeeDaolmpl();
        logger.info("Create table");
        sdi.createEmployeeTable();
        logger.info("Insert a new record");
        Employee employee = new Employee()
                .setId(expectedId)
                .setLastName("Lopatka").setFirstName("Ostap")
                .setMiddleName("Severynovych").setPoint("m")
                .setBirthdayDay("1995-12-01").setParentsName("Lopanka Severyn Valentinovych")
                .setPhone("11122233").setPassport("CC0909CC")
                .setRecordBookNumber("3114049c").setArrivalDate("2016-09-01");
        sdi.insert(employee);
        logger.info("Select by id");
        Employee employeeSelect = sdi.selectById(expectedId);
        logger.info(employee.getId() + ", " + employee.getFirstName() + ", " + employee.getLastName());
        assertEquals("Employee was not added", expectedId, employeeSelect.getId());
    }

    @Test
    public void checkUpdateEmployee() {
        String expectedLastName = "Grabla";
        EmployeeDaolmpl sdi = new EmployeeDaolmpl();
        logger.info("Select by id");
        Employee employeeSelect = sdi.selectById(expectedId);
        Employee personUpdate = employeeSelect
                .setLastName(expectedLastName);
        sdi.update(personUpdate, employeeSelect.getId());
        assertEquals("Employee was not update", personUpdate.getLastName(), expectedLastName);
    }

    @Test
    public void checkResults() {
        EmployeeDaolmpl sdi = new EmployeeDaolmpl();
        logger.info("Select all persons");
        List<Employee> employees = sdi.selectAll();
        for (Employee p : employees) {
            logger.info(p.getId() + ", " + p.getFirstName() + ", " + p.getLastName());
        }
        assertTrue("Select all employees", employees.size() > 0);
    }

    @After
    public void deleteData() {
        EmployeeDaolmpl sdi = new EmployeeDaolmpl();
        sdi.delete(expectedId);
        sdi.delete(initialEmployee);
    }
}
