package com.epam.lab.transactions;

import com.epam.lab.App;
import com.epam.lab.util.ConnectionConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MyTransaction {
    private static Logger logger = LogManager.getLogger(App.class);

    public void getTransaction() throws SQLException {
        Connection dbConnection = null;
        PreparedStatement preparedStatementInsert = null;
        PreparedStatement preparedStatementUpdate = null;
        String insertTableSQL = "INSERT INTO department " + "VALUES (?, ?, ?)";
        String updateTableSQL = "UPDATE department SET " +
                "speciality_name = ?, describe_speciality = ? WHERE idSpeciality = ?";
        try {
            dbConnection = ConnectionConfiguration.getConnection();
            dbConnection.setAutoCommit(false);
            preparedStatementInsert = dbConnection.prepareStatement(insertTableSQL);
            preparedStatementInsert.setInt(1, 999);
            preparedStatementInsert.setString(2, "ER");
            preparedStatementInsert.setString(3, "systems");
            preparedStatementInsert.executeUpdate();
            preparedStatementUpdate = dbConnection.prepareStatement(updateTableSQL);
            preparedStatementUpdate.setString(1, "RE");
            preparedStatementUpdate.setString(2, "philosophy");
            preparedStatementUpdate.setInt(3, 998);
            preparedStatementUpdate.executeUpdate();
            dbConnection.commit();
            logger.info("Done!");
        } catch (SQLException e) {
            logger.info(e.getMessage());
            dbConnection.rollback();
        } finally {
            if (preparedStatementInsert != null) {
                preparedStatementInsert.close();
            }
            if (preparedStatementUpdate != null) {
                preparedStatementUpdate.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
    }
}
