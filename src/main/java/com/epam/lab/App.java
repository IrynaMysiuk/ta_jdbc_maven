package com.epam.lab;

import com.epam.lab.daoimpl.DepartmentDaoImpl;
import com.epam.lab.daoimpl.EmployeeDaolmpl;
import com.epam.lab.entities.Department;
import com.epam.lab.entities.Employee;
import com.epam.lab.metadata.MyConsoleSQL;
import com.epam.lab.transactions.MyTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;

public class App {
    private static Logger logger = LogManager.getLogger(App.class);

    public static void main(String[] args) throws SQLException {
        EmployeeDaolmpl sdi = new EmployeeDaolmpl();
        logger.info("Create table");
        sdi.createEmployeeTable();
        logger.info("Insert a new record");
        Employee employee = new Employee()
                .setLastName("Lopatka").setFirstName("Ostap")
                .setMiddleName("Severynovych").setPoint("m")
                .setBirthdayDay("1995-12-01").setParentsName("Lopanka Severyn Valentinovych")
                .setPhone("11122233").setPassport("CC0909CC")
                .setRecordBookNumber("3114049c").setArrivalDate("2016-09-01");
        sdi.insert(employee);
        logger.info("Select by id");
        Employee employeeSelect = sdi.selectById(2);
        logger.info(employee.getId() + ", " + employee.getFirstName() + ", " + employee.getLastName());
        logger.info("Delete person by id");
        sdi.delete(2);
        logger.info("Update person");
        Employee personUpdate = new Employee()
                .setLastName("Grabla").setFirstName("Kyryl")
                .setMiddleName("Vasiliovych").setPoint("m")
                .setBirthdayDay("1995-12-01").setParentsName("Grabla Vasyl Orestovych")
                .setPhone("2221133").setPassport("DD0909DD")
                .setRecordBookNumber("3114079c").setArrivalDate("2018-09-01");
        sdi.update(personUpdate, 1);
        logger.info("Select all persons");
        List<Employee> employees = sdi.selectAll();
        for (Employee p : employees) {
            logger.info(p.getId() + ", " + p.getFirstName() + ", " + p.getLastName());
        }
        DepartmentDaoImpl ddi = new DepartmentDaoImpl();
        logger.info("Create table");
        ddi.createDepartmentTable();
        logger.info("Insert a new record");
        Department department = new Department("Fei", "electronics");
        ddi.insert(department);
        logger.info("Select by id");
        Department departmentSelect = ddi.selectById(2);
        logger.info(department.getId() + ", " + department.getSpecialityName() + ", " + department.getDescribeSpeciality());
        logger.info("Delete person by id");
        ddi.delete(1);
        logger.info("Update person");
        Department departmentUpdate = new Department("KH", "computer science");
        ddi.update(departmentUpdate, 1);
        logger.info("Select all persons");
        List<Department> departments = ddi.selectAll();
        for (Department p : departments) {
            logger.info(p.getId() + ", " + p.getSpecialityName() + ", " + p.getDescribeSpeciality());
        }
        logger.info("Transaction");
        MyTransaction transaction = new MyTransaction();
        transaction.getTransaction();
        MyConsoleSQL consoleSQL = new MyConsoleSQL();
        consoleSQL.getTablesMetaData();
        consoleSQL.getColumnsNameMetaData("department");
        consoleSQL.getAllColumnMetaData("department");
    }
}