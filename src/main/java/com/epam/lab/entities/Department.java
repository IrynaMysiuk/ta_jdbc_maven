package com.epam.lab.entities;

public class Department {

    private int idSpeciality;
    private String specialityName;
    private String describeSpeciality;

    public Department() {
    }

    public Department(String specialityName, String describeSpeciality) {
        this.specialityName = specialityName;
        this.describeSpeciality = describeSpeciality;
    }

    public int getId() {
        return idSpeciality;
    }

    public Department setId(int idSpeciality) {
        this.idSpeciality = idSpeciality;
        return this;
    }

    public String getSpecialityName() {
        return specialityName;
    }

    public Department setSpecialityName(String specialityName) {
        this.specialityName = specialityName;
        return this;
    }

    public String getDescribeSpeciality() {
        return describeSpeciality;
    }

    public void setDescribeSpeciality(String describeSpeciality) {
        this.describeSpeciality = describeSpeciality;
    }

    @Override
    public String toString() {
        return idSpeciality + ", " + specialityName + ", " + describeSpeciality;
    }
}